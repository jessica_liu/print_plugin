package com.bster.print;

import android.util.Log;
import android.widget.Toast;

import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniModule;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import io.dcloud.common.util.ReflectUtils;

public class PrintUtil extends UniModule {
    String TAG = "TestModule-----------------------------";

    @UniJSMethod(uiThread = false)
    public void printBoxBarcode(JSONObject options, UniJSCallback callback) {
        Log.e(TAG, "rest -data--------------" + options);
        JSONObject data = new JSONObject();
        try {

            data.put("code", "success");
            if (!SunmiPrintHelper.getInstance().isLabelMode()) {
                data.put("code", "0");
                data.put("message", "请调整打印机为标签打印模式！");
            }
            JSONArray array = options.getJSONArray("codeList");
            boolean flag = SunmiPrintHelper.getInstance().printBoxLabel(array);
            if (!flag) {
                data.put("code", "0");
                data.put("message", "打印机异常！");
            } else {
                data.put("code", "1");
                data.put("message", "打印成功！");
            }
        } catch (Exception e) {
            data.put("code", "0");
            data.put("message", e.getMessage());
        } finally {

            if (callback != null) {
                callback.invoke(data);
            }
        }

    }

    @UniJSMethod(uiThread = false)
    public void printTurnoverLabel(JSONObject options, UniJSCallback callback) {
        Log.e(TAG, "rest -data--------------" + options);
        JSONObject data = new JSONObject();
        try {

            data.put("code", "success");
            if (!SunmiPrintHelper.getInstance().isLabelMode()) {
                data.put("code", "0");
                data.put("message", "请调整打印机为标签打印模式！");
            }
            JSONArray array = options.getJSONArray("codeList");
            boolean flag = SunmiPrintHelper.getInstance().printTurnoverLabel(array);
            if (!flag) {
                data.put("code", "0");
                data.put("message", "打印机异常！");
            } else {
                data.put("code", "1");
                data.put("message", "打印成功！");
            }
        } catch (Exception e) {
            data.put("code", "0");
            data.put("message", e.getMessage());
        } finally {

            if (callback != null) {
                callback.invoke(data);
            }
        }

    }

    @UniJSMethod(uiThread = false)
    public void initPrinter(UniJSCallback callback) {
        JSONObject data = new JSONObject();
        data.put("code", "success");
        SunmiPrintHelper.getInstance().initSunmiPrinterService(ReflectUtils.getApplicationContext());
        if (callback != null) {
            callback.invoke(data);
        }
    }

    @UniJSMethod(uiThread = false)
    public void destoryPrinter(UniJSCallback callback) {
        JSONObject data = new JSONObject();
        data.put("code", "success");
        SunmiPrintHelper.getInstance().deInitSunmiPrinterService(ReflectUtils.getApplicationContext());
        if (callback != null) {
            callback.invoke(data);
        }
    }

    @UniJSMethod(uiThread = false)
    public void getPrinterStatus(UniJSCallback callback) {
        JSONObject data = SunmiPrintHelper.getInstance().showPrinterStatus();
        if (callback != null) {
            callback.invoke(data);
        }
    }
}
