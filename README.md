# print_plugin

#### 介绍
对接sunmi内置打印机打印插件


#### 使用说明
此插件目前只实现了uniapp android端适配 sunmi v2s设备，文档支持如下
1.  https://nativesupport.dcloud.net.cn/NativePlugin/course/android.html
2.  https://nativesupport.dcloud.net.cn/NativePlugin/course/package.html#

